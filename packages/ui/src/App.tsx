import React from 'react';
import {Button, ButtonModes, ButtonSize} from "./button";

function App() {
  return (
    <div className="App">
        <Button onClick={() => console.log('b')} mode={ButtonModes.BOLD} size={ButtonSize.SMALL}>Test</Button>
    </div>
  );
}

export default App;
