import { render, screen } from '@testing-library/react';
import {Button, ButtonModes} from "./Button";

describe('Button', () => {
    test('basic', () => {
        render(<Button>TEST</Button>);
        expect(screen.getByText('TEST')).toBeInTheDocument();
    });
    test('mode SCALE', () => {
        render(<Button mode={ButtonModes.SCALE}>TEST</Button>);
        expect(screen.getByText('TEST')).toHaveClass('scale');
        screen.debug();
    });
});
