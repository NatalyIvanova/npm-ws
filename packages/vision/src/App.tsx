import React from 'react';
import './App.css';
import {Button, ButtonModes} from "@npm-ws/ui";
import {ButtonSize} from "@npm-ws/ui/src/button/ui/Button";

function App() {
  return (
    <div className="App">
        <Button onClick={() => console.log('b')} mode={ButtonModes.BOLD} size={ButtonSize.SMALL}>Test</Button>
    </div>
  );
}

export default App;
